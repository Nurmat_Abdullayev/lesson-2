// Convert the switch statement into an object called lookup.
// Use it to look up val and assign the associated string to the result variable.

// ! You should not use case, switch, or if statements

function phoneticLookup(val) {
	const lookup = {
		"alpha": "Adams",
		"bravo": "Boston",
		"charlie": "Chicago",
		"delta": "Denver",
		"echo": "Easy",
	};
	let result = null
	result = lookup[val];

	return result;
}

console.log(phoneticLookup("bravo"))
console.log(phoneticLookup("alpha"))
console.log(phoneticLookup("delta"))


// Write two ways of creating objects using functions that represent
// the items of the products on the products.jpg on the source directory

// Factory Function
// You code here ...

// Constructor Function
// You code here ...
const productCreator = (name, price, title, quantity) => {
	return {
		name,
		price,
		title,
		quantity,
		get totalPrice() {
			return this.price * this.quantity
		},
		set Price(price) {
			this.price = price
		}
	}
}


const product = productCreator('Max Box', 35000, 'Popular', 2)
const totalPrice = product.totalPrice
console.log(totalPrice)






function Product(name, price, title, quantity) {
	this.name = name;
	this.price = price;
	this.title = title;
	this.quantity = quantity;
	this.getTotalPrice = function () {
		return this.price * this.quantity
	},
		this.setPrice = function (price) {
			this.price = price
		}
}

const product1 = new Product('Max Box', 35000, 'Popular', 2)
const product2 = new Product('Max Box', 30000, 'Retro', 3)
const product3 = new Product('Max Box', 30000, 'Trend', 5)


console.log(product1)
console.log(product2)
console.log(product3.setPrice(100))
console.log(product3.getTotalPrice())

